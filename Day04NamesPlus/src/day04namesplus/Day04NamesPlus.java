package day04namesplus;

import java.util.ArrayList;
import java.util.Scanner;

public class Day04NamesPlus {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ArrayList<String> nameList = new ArrayList<>();
        //
        while (true) {
            System.out.print("Enter a name, empty to finish: ");
            String name = input.nextLine();
            if (name.isEmpty()) {
                break;
            }
            nameList.add(name);
        }
        //
        for (int i = 0; i < nameList.size(); i++) {
            System.out.printf("%s%s", i == 0 ? " " : ";", nameList.get(i));
        }
        System.out.println("");
    }
    
}
