package day08todos;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;



class Todo {

    public Todo(String task, Date dueDate, int hoursOfWork, TaskStatus status) throws InvalidDataException {
        setTask(task);
        setDueDate(dueDate);
        setHoursOfWork(hoursOfWork);
        setStatus(status);
    }

    public Todo(String dataLine) throws InvalidDataException {
        try {
            String[] data = dataLine.split(";");
            if (data.length != 4) {
                throw new InvalidDataException("Error: invalid number of items in line: " + dataLine);
            }
            setTask(data[0]); // IllegalArgumentException
            setDueDate(dateFormatFile.parse(data[1])); // ParseException
            setHoursOfWork(Integer.parseInt(data[2])); // NumberFormatException
            setStatus(TaskStatus.valueOf(data[3])); // IllegalArgumentException
        } catch (IllegalArgumentException | ParseException ex) {
            // exception chaining
            throw new InvalidDataException("Error: parsing failed", ex);
        }
    }

    enum TaskStatus {
        Pending, Done
    };

    private String task; // 2-50 characters long, must NOT contain a semicolon or | or ` (reverse single quote) characters
    private Date dueDate; // Date between year 1900 and 2100
    private int hoursOfWork; // 0 or greater number
    private TaskStatus status;

    // format all fields of this Todo item for display exactly as specified below in the example interactions
    @Override
    public String toString() {
        String dueDateStr = dateFormatScreen.format(dueDate);
        return String.format("%s, %s, will take %d hour(s) of work, status: %s.",
                task, dueDateStr, hoursOfWork, status);
    }

    public String toDataString() {
        String dueDateStr = dateFormatFile.format(dueDate);
        return String.format("%s;%s;%d;%s", task, dueDateStr, hoursOfWork, status);
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) throws InvalidDataException {
        if (!task.matches("[^|`;]*")) {
            throw new InvalidDataException("Task must not contain ; ` | characters");
        }
        if (task.length() < 2 || task.length() > 50) {
            throw new InvalidDataException("Task must be between 2-50 chars long");
        }
        this.task = task;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) throws InvalidDataException {
        // TODO: find a way to check year, suggestion use Calendar class
        //int year = dueDate.getYear();
        //if (year < 0 || year > 200) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dueDate);
        int year = cal.get(Calendar.YEAR);
        if (year < 1900 | year > 2100) {
            throw new InvalidDataException("Year must be between 1900 and 2100");
        }
        this.dueDate = dueDate;
    }

    public int getHoursOfWork() {
        return hoursOfWork;
    }

    public void setHoursOfWork(int hoursOfWork) throws InvalidDataException {
        if (hoursOfWork < 0) {
            throw new InvalidDataException("Hours of work can't be less than 0");
        }
        this.hoursOfWork = hoursOfWork;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public void setStatus(String statusStr) throws InvalidDataException {
        try {
            this.status = TaskStatus.valueOf(statusStr);
        } catch (IllegalArgumentException ex) {
            throw new InvalidDataException("Invalid status enum value: " + statusStr, ex);
        }
    }

    final static SimpleDateFormat dateFormatFile = new SimpleDateFormat("yyyy-MM-dd");
    final static SimpleDateFormat dateFormatScreen = new SimpleDateFormat("yyyy/MM/dd");
}

public class Day08Todos {

    static int inputInt() {
        while (true) {
            try {
                int value = input.nextInt();
                input.nextLine();
                return value;
            } catch (InputMismatchException ex) {
                System.out.print("Invalid integer, please try again: ");
                input.nextLine();
            }
        }
    }

    static int getMenuChoice() {
        System.out.print("Please make a choice [0-4]:\n"
                + "1. Add a todo\n"
                + "2. List all todos (numbered)\n"
                + "3. Delete a todo\n"
                + "4. Modify a todo\n"
                + "0. Exit\n"
                + "Your choice is: ");
        int choice = inputInt();
        input.nextLine();
        return choice;
    }

    static ArrayList<Todo> todoList = new ArrayList<>();
    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        loadDataFromFile();
        while (true) {
            int choice = getMenuChoice();
            switch (choice) {
                case 1:
                    addTodo();
                    break;
                case 2:
                    listAllTodos();
                    break;
                case 3:
                    deleteTodo();
                    break;
                case 4:
                    modifyTodo();
                    break;
                case 0:
                    saveDataToFile();
                    System.out.println("Exiting. Good bye!");
                    return;
                default:
                    System.out.println("Invalid choice, try again.");
            }
            System.out.println();
        }
    }

    private static void addTodo() {
        try {
            System.out.println("Adding a todo.");
            System.out.print("Enter task description: ");
            String task = input.nextLine();
            System.out.print("Enter due Date (yyyy/mm/dd): ");
            String dueDateStr = input.nextLine();
            Date dueDate = Todo.dateFormatScreen.parse(dueDateStr);
            System.out.print("Enter hours of work (integer): ");
            int hours = inputInt();
            Todo todo = new Todo(task, dueDate, hours, Todo.TaskStatus.Pending);
            todoList.add(todo);
            System.out.println("You've created the following todo:");
            System.out.println(todo);
        } catch (ParseException ex) {
            System.out.println("Error parsing: " + ex.getMessage());
        } catch (IllegalArgumentException | InvalidDataException ex) {
            System.out.println("Data error: " + ex.getMessage());
        }
    }

    private static void listAllTodos() {
        if (todoList.isEmpty()) {
            System.out.println("There are no todos.");
            return;
        }
        for (int i = 0; i < todoList.size(); i++) {
            System.out.printf("#%d: %s\n", i + 1, todoList.get(i));
        }
    }

    private static void deleteTodo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static void modifyTodo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    final static String FILE_NAME = "todos.txt";

    static void loadDataFromFile() {
        try ( Scanner inputFile = new Scanner(new File(FILE_NAME))) {
            while (inputFile.hasNextLine()) {
                try {
                    String line = inputFile.nextLine();
                    Todo todo = new Todo(line);
                    todoList.add(todo);
                } catch (InvalidDataException ex) {
                    // ex.printStackTrace();
                    System.out.println("Error parsing line, skipping: " + ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
    }

    static void saveDataToFile() {
        try ( PrintWriter fileOutput = new PrintWriter(new File("x" + FILE_NAME))) {
            for (Todo todo : todoList) {
                fileOutput.println(todo.toDataString());
            }
        } catch (IOException ex) {
            System.out.println("Error writing file: " + ex.getMessage());
        }
    }

}
