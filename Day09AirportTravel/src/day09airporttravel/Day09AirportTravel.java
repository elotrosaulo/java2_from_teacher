package day09airporttravel;

class ParameterInvalidException extends RuntimeException {

    public ParameterInvalidException(String message) {
        super(message);
    }
}

class Airport {

    public Airport(String dataLine) {
        String[] data = dataLine.split(";");
        if (data.length != 4) {
            throw new ParameterInvalidException("Invalid number of fields in line: " + dataLine);
        }
        setCode(data[0]);
        setCity(data[1]);
        setLatitude(Double.parseDouble(data[2])); // throws NumberFormatException
        setLongitude(Double.parseDouble(data[3])); // throws NumberFormatException
    }

    public Airport(String code, String city, double latitude, double longitude) {
        setCode(code);
        setCity(city);
        setLatitude(latitude);
        setLongitude(longitude);
    }

    private String code;
    private String city;
    private double latitude;
    private double longitude;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        if (!code.matches("[A-Z]{3}")) {
            throw new ParameterInvalidException("Airport code must be exactly 3 uppercase letters");
        }
        this.code = code;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        if (city.isEmpty() || city.contains(";")) {
            throw new ParameterInvalidException("City must not be emtpy and can not contain semicolons");
        }
        this.city = city;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
    
    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

}

public class Day09AirportTravel {

    public static void main(String[] args) {
        // TODO code application logic here
    }

}
