package day09geometryfromfile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

class DataInvalidException extends Exception {

    public DataInvalidException(String msg) {
        super(msg);
    }

    public DataInvalidException(String msg, Throwable cause) {
        super(msg, cause);
    }

}

abstract class GeoObj {

    // factory method
    static GeoObj createFromLine(String dataLine) throws DataInvalidException {
        try {
            String[] data = dataLine.split(";");
            switch (data[0]) {
                case "Rectangle": {
                    if (data.length != 4) {
                        throw new DataInvalidException("Error: invalid number of items in line: " + dataLine);
                    }
                    String color = data[1];
                    double width = Double.parseDouble(data[2]);
                    double height = Double.parseDouble(data[3]);
                    return new Rectangle(color, width, height);
                }
                case "Square": {
                    if (data.length != 3) {
                        throw new DataInvalidException("Error: invalid number of items in line: " + dataLine);
                    }
                    String color = data[1];
                    double edgeSize = Double.parseDouble(data[2]);
                    return new Square(color, edgeSize);
                }
                case "Circle": {
                    if (data.length != 3) {
                        throw new DataInvalidException("Error: invalid number of items in line: " + dataLine);
                    }
                    String color = data[1];
                    double radius = Double.parseDouble(data[2]);
                    return new Circle(color, radius);
                }
                case "Point": {
                    if (data.length != 2) {
                        throw new DataInvalidException("Error: invalid number of items in line: " + dataLine);
                    }
                    String color = data[1];
                    return new Point(color);
                }
                default:
                    throw new DataInvalidException("Error: don't know how to make " + dataLine);
            }
        } catch (NumberFormatException ex) {
            throw new DataInvalidException("Parsing error", ex);
        }

    }

    GeoObj(String color) throws DataInvalidException {
        if (!color.matches("[A-Za-z -]+")) {
            throw new DataInvalidException("Color invalid");
        }
        this.color = color;
    }

    abstract double getSurface();

    abstract double getCircumPerim();

    abstract int getVerticesCount();

    abstract int getEdgesCount();

    private String color;

    String getColor() {
        return color;
    }

    @Override
    public String toString() {
        return String.format("%s: color=%s, surface=%.2f, circperim=%.2f, verts=%d, edges=%d",
                getClass().getSimpleName(), color, getSurface(), getCircumPerim(), getVerticesCount(), getEdgesCount());
    }
}

class Point extends GeoObj {

    Point(String color) throws DataInvalidException {
        super(color);
    }

    @Override
    double getSurface() {
        return 0;
    }

    @Override
    double getCircumPerim() {
        return 0;
    }

    @Override
    int getVerticesCount() {
        return 1;
    }

    @Override
    int getEdgesCount() {
        return 0;
    }
}

class Rectangle extends GeoObj {

    Rectangle(String color, double width, double height) throws DataInvalidException {
        super(color);
        if (width < 0 || height < 0) {
            throw new DataInvalidException("width and height must not be negative");
        }
        this.width = width;
        this.height = height;
    }

    private double width, height;

    @Override
    double getSurface() {
        return width * height;
    }

    @Override
    double getCircumPerim() {
        return 2 * width + 2 * height;
    }

    @Override
    int getVerticesCount() {
        return 4;
    }

    @Override
    int getEdgesCount() {
        return 4;
    }

    @Override
    public String toString() {
        String parentResult = super.toString();
        return parentResult + String.format(", width=%.2f, height=%.2f", width, height);
    }

}

class Square extends Rectangle {

    public Square(String color, double edgeSize) throws DataInvalidException {
        super(color, edgeSize, edgeSize);
    }

}

class Circle extends GeoObj {

    Circle(String color, double radius) throws DataInvalidException {
        super(color);
        if (radius < 0) {
            throw new DataInvalidException("radius must not be negative");
        }
        this.radius = radius;
    }

    private double radius;

    @Override
    double getSurface() {
        return Math.PI * radius * radius;
    }

    @Override
    double getCircumPerim() {
        return radius * 2 * Math.PI;
    }

    @Override
    int getVerticesCount() {
        return 0;
    }

    @Override
    int getEdgesCount() {
        return 1;
    }

    @Override
    public String toString() {
        String parentResult = super.toString();
        return parentResult + String.format(", radius=%.2f", radius);
    }

}

// TODO: sphere, hexagon
public class Day09GeometryFromFile {

    static ArrayList<GeoObj> geoList = new ArrayList<>();
    
    public static void main(String[] args) {
        try ( Scanner fileInput = new Scanner(new File("objects.txt"))) {
            while (fileInput.hasNextLine()) {
                try {
                    String line = fileInput.nextLine();
                    GeoObj geoObj;
                    geoObj = GeoObj.createFromLine(line); // exceptions ?
                    geoList.add(geoObj);
                    System.out.println(geoObj);
                } catch (DataInvalidException ex) {
                    System.out.println("Error parsing line: " + ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }

        /*
        try {
            Point p1 = new Point("green");
            System.out.println(p1);
            Rectangle r1 = new Rectangle("yellow", 10, 2.56);
            System.out.println(r1);
            Square sq1 = new Square("orange", 3.2);
            System.out.println(sq1);
            Circle c1 = new Circle("blue", 6.7);
            System.out.println(c1);
            
        } catch (DataInvalidException ex) {
            System.out.println("Fatal exception: " + ex.getMessage());
        } */
    }

}
