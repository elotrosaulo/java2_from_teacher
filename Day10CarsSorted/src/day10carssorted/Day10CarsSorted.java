package day10carssorted;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

class Car implements Comparable<Car> {

    public Car(String makeModel, double engineSizeL, int prodYear) {
        this.makeModel = makeModel;
        this.engineSizeL = engineSizeL;
        this.prodYear = prodYear;
    }

    String makeModel;
    double engineSizeL;
    int prodYear;

    @Override
    public String toString() {
        return "Car{" + "makeModel=" + makeModel + ", engineSizeL=" + engineSizeL + ", prodYear=" + prodYear + '}';
    }

    @Override
    public int compareTo(Car otherCar) {
        return makeModel.compareTo(otherCar.makeModel);
    }

    static Comparator<Car> CompareByEngineSize = new Comparator<>() {
        @Override
        public int compare(Car c1, Car c2) {
            // WRONG: return (int)(c1.engineSizeL - c2.engineSizeL);
            if (c1.engineSizeL == c2.engineSizeL) {
                return 0;
            }
            if (c1.engineSizeL > c2.engineSizeL) {
                return 1;
            } else {
                return -1;
            }
        }
    };
    
    static Comparator<Car> CompareByProdYear = new Comparator<>() {
        @Override
        public int compare(Car c1, Car c2) {
            return c1.prodYear - c2.prodYear;
        }
    };
    
    static Comparator<Car> CompareByProdYearThenMakeModel = new Comparator<>() {
        @Override
        public int compare(Car c1, Car c2) {
            int result = c1.prodYear - c2.prodYear;
            if (result != 0) return result;
            return c1.makeModel.compareTo(c2.makeModel);
        }
    };
    
}

public class Day10CarsSorted {

    static ArrayList<Car> parking = new ArrayList<>();

    public static void main(String[] args) {
        parking.add(new Car("Toyota Corolla", 1.8, 2011));
        parking.add(new Car("BMW X5", 3.2, 2017));
        parking.add(new Car("VW Golf", 1.4, 2011));
        parking.add(new Car("Audi A4", 2.1, 2015));
        parking.add(new Car("Nissan", 2.2, 2011));
        System.out.println("======== ORIGINAL ORDER:");
        for (Car c : parking) {
            System.out.println(c);
        }
                System.out.println("======== Sorted by prodYear then makeModel");
        Collections.sort(parking, Car.CompareByProdYear);
        for (Car c : parking) {
            System.out.println(c);
        }
        System.out.println("======== Sorted by makeModel");
        Collections.sort(parking);
        for (Car c : parking) {
            System.out.println(c);
        }
        System.out.println("======== Sorted by engineSizeL");
        Collections.sort(parking, Car.CompareByEngineSize);
        for (Car c : parking) {
            System.out.println(c);
        }
        System.out.println("======== Sorted by prodYear");
        Collections.sort(parking, Car.CompareByProdYear);
        for (Car c : parking) {
            System.out.println(c);
        }

    }

}
