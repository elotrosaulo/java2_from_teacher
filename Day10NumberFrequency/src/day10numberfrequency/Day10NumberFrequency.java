package day10numberfrequency;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

class NumFreq implements Comparable<NumFreq> {

    public NumFreq(int number, int frequency) {
        this.number = number;
        this.frequency = frequency;
    }

    int number;
    int frequency;

    @Override
    public String toString() {
        return number + "=>" + frequency;
    }

    @Override
    public int compareTo(NumFreq other) {
        return other.frequency - frequency;
    }
        
    
}

public class Day10NumberFrequency {

    // ALTERNATIVE: static HashMap<Integer, Integer> numFreqMap = new HashMap<>();
    
    static ArrayList<Integer> intList = new ArrayList<>();
    static ArrayList<NumFreq> numFreqList = new ArrayList<>();

    public static void main(String[] args) {
        try ( Scanner fileInput = new Scanner(new File("numbers.txt"))) {
            while (fileInput.hasNextInt()) { // FIXME: skip invalid numbers
                intList.add(fileInput.nextInt());
            }
        } catch (IOException ex) {
            System.out.println("Error reading file");
            return;
        }
        // NOTE: demonstrate how to shorten this using equals() override
        for (int n : intList) {
            // is n on numFreqList ?
            NumFreq foundNF = null;
            for (NumFreq nf : numFreqList) {
                if (nf.number == n) {
                    foundNF = nf;
                    break;
                }
            }
            // foundNF is null if not found, otherwise it points to nf.number == n
            if (foundNF == null) { // haven't seen this number before
                NumFreq newNF = new NumFreq(n, 1);
                numFreqList.add(newNF);
            } else { // already on the list
                foundNF.frequency++;
            }
        }
        //
        System.out.println("FreqList: " + numFreqList);
        //
        Collections.sort(numFreqList);
        System.out.println("Number | Occurrences \n" + "-------+------------");
        for (NumFreq nf : numFreqList) {
            System.out.printf("%6d | %10d\n", nf.number, nf.frequency);
        }
    }

}
