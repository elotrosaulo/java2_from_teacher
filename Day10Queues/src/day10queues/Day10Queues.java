package day10queues;

import java.util.ArrayList;

class LIFOStack<T> {

    private ArrayList<T> storage = new ArrayList<>();

    void push(T item) {
        storage.add(0, item);
    }

    T pop() { // throw IndexOutOfBoundsException on empty
        if (storage.isEmpty()) {
            throw new IndexOutOfBoundsException("Stack empty");
        }
        return storage.remove(0);
    }

    T peek() { // returns top of the stack without taking it off
        if (storage.isEmpty()) {
            throw new IndexOutOfBoundsException("Stack empty");
        }
        return storage.get(0);
    }

    int size() {
        return storage.size();
    }
    // also implement toString that prints all items in a single line, comma-separated

    @Override
    public String toString() {
        return "LIFOStack" + storage.toString();
    }

}

class FIFOQueue<T> {

    private ArrayList<T> storage = new ArrayList<>();

    void add(T item) {
        storage.add(item);
    }

    T remove() { // throws IndexOutOfBoundsException on empty
        if (storage.isEmpty()) {
            throw new IndexOutOfBoundsException("Queue empty");
        }
        return storage.remove(0);
    }

    int size() {
        return storage.size();
    }

    @Override
    public String toString() {
        return "LIFOQueue" + storage.toString();
    }
}

public class Day10Queues {

    public static void main(String[] args) {
        FIFOQueue<String> queue = new FIFOQueue<>();
        queue.add("Jerry");
        queue.add("Terry");
        queue.add("Barry");
        System.out.println(queue);
        String name1 = queue.remove();
        String name2 = queue.remove();
        System.out.println(queue);
        System.out.println("-------------");
        LIFOStack<String> stack = new LIFOStack<>();
        stack.push("Java Programming");
        stack.push("C# programming");
        stack.push("JS Programming");
        stack.push("Databases");
        System.out.println(stack);
        String book1 = stack.pop();
        String book2 = stack.pop();
        System.out.println(stack);
        String book3 = stack.pop();
        String book4 = stack.pop();
        String book5 = stack.pop();
        System.out.println(stack);

    }

}
